# itwkracing-obs

Simple app that connects OBS and iRacing, to auto stream....

## What it does

Based on what is happening in iRacing it will select the following scene:

* when the simulator starts, the Intro scene is displayed
* starts streaming/recording
* when the car is on track, the DrivingCar scene is displayed
* when the car is in the garage, the Garage scene is displayed
* once the race is completed, the Final scene is displayed
* stops streaming/recording

### Web Resources

* http://localhost:3000/ - help page
* https://localhost:3000/sponsors - shows sponsors from public/images/sponsors
* https://localhost:3000/sponsor - rotates through sponsors from public/images/sponsors
* https://localhost:3000/driver - shows Iracing / name from iracingbanner.com
* https://localhost:3000/drivers - shows list of drivers

## Required Scenes

* Intro
* DrivingCar
* Garage
* Final

### Monitoring Telemetry Data

* T.values.IsOnTrack
* T.values.IsinPit
* T.values.OnPitRoad
* T.values.PlayerCarInPitStall
* T.values.CarIdxOnPitRoad
* T.values.CarIdxTrackSurface
* T.values.CarIdxTrackSurfaceMaterial

## Install

1. [install nodejs](https://nodejs.org) version 12 (NOT 14 currently)
2. [install obs](https://obsproject.com/download)
3. [install obs-socket plugin](https://github.com/Palakis/obs-websocket)

```bash
git clone https://twk-b@bitbucket.org/twk-b/itwkracing-obs.git
cd itwkracing-obs
npm install
```

1. Dump images for sponsors into public/sponsors
1. Copy example OBS config scene file docs/iracing.json to C:\Users\USERNAME\AppData\Roaming\obs-studio\basic\scenes\

## Use

Start OBS, then:

```bash
npm start
```

Then forget about it, and sim away. iTwkRacing-obs will start and stop your stream with each sim session.

## Use With

* [iTwkRacing for overlays](https://itwkracing.dcbl.ca)

## Road Map

* Add Post Qualify Page
* Improve UI
* Intro
    * show what time the qualifier/race starts
* Add Qualify
    * show qualifying results
* Final
    * show final results
* Config
    * only stream qualifying/race
    * configurable stop stream time (currently 30s)
    * configurable rotation time for ads (currently 30s)
* fastest lap dashboard pop up
* who is in the driver window
* interview page
    * name who is in the interview
* strip of names for top bottom, cycle through 5 at a time
* 2 onscreen drivers info

## Sponsored by

* [dcbl.ca](https://dcbl.ca)
* [SimRace247.com](https://simrace247.com)

## Resources

* https://github.com/Palakis/obs-websocket
* https://github.com/haganbmj/obs-websocket-js
* https://github.com/nodecg/nodecg-obs
* https://github.com/GamesDoneQuick/agdq17-layouts
* https://www.youtube.com/watch?v=r3IR3fkqmSI
* https://www.youtube.com/watch?v=stMBmiEwvYE

## Pro shops that do racecasts

* RaceSpot
* Podium

## TODO

* button to create required Scenes if they dont exist
* button to stop/start recording
* endpoint to hit