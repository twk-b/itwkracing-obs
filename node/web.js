const express = require('express');
const glob = require('glob');
const bodyParser = require('body-parser');
const _ = require('lodash');
const path = require('path');
const urlJoin = require('url-join');

const app = express();

const width = 1080;
const port = process.env.PORT || 3000;
exports.images = [];
exports.ads = [];
exports.driver = {
  DriverUserID: 357953,
  Drivers: []
};
exports.perRow = 3;

exports.getData = () => {
  const sponsorPath = path.join(__dirname, '../public/');
  glob(`${sponsorPath}images/**`, { nodir: true }, (err, images) => {
    if (err) {
      console.error('getData images', err);
    } else {
      console.log('images:', images.length, sponsorPath);
      exports.images = _.map(images, (img) => urlJoin('/images/sponsors/', path.basename(img)));
    }
  });
};
exports.getData();

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '../views/'));
app.use(express.static(path.join(__dirname, '../public')));
app.use(bodyParser.json());

app.use((err, req, res, next) => {
  console.error('err', err, err.stack);
  next();
});

function logErrors(err, req, res, next) {
  console.error(err.stack);
  next(err);
}
app.use(logErrors);

app.post('/driver', (req, res) => {
  if (req.body) {
    console.log('driver req.body', _.keys(req.body));
    exports.driver = req.body;
    res.send('driver updated');
  } else {
    res.status(400).send('failed to update driver');
  }
});

exports.getLocations = (drivers) => {
  const locations = _.uniq(_.map(drivers, 'ClubName'));
  let locs = {};
  _.each(locations, (loc) => {
    locs[loc] = _.filter(drivers, { ClubName: loc }).length;
  });
  locs = _.fromPairs(_.sortBy(_.toPairs(locs), 1).reverse());
  return locs;
};
exports.getStrenthOfField = (drivers) => {
  return parseInt((_.reduce(drivers, (sum, driver) => {
    sum += driver.IRating;
    return sum;
  }, 0) / exports.driver.Drivers.length).toFixed(0), 10);
};

app.get('/', (req, res) => res.render('index', { driver: _.omit(exports.driver, 'Drivers'), drivers: exports.driver.Drivers, message: 'itwkracing-obs running!', images: exports.images, ads: exports.ads }));
app.get('/driver', (req, res) => res.render('driver', { driver: exports.driver }));
app.get('/drivers', (req, res) => {

  exports.driver.Drivers = _.reverse(_.sortBy(exports.driver.Drivers, 'IRating'));
  res.render('drivers', {
    locations: exports.getLocations(exports.driver.Drivers),
    SOF: exports.getStrenthOfField(exports.driver.Drivers),
    driver: exports.driver
  });
});
app.get('/sponsor', (req, res) => {
  exports.getData();
  res.render('sponsor', {
    images: exports.images,
  });
});
app.get('/sponsors', (req, res) => {
  exports.getData();
  res.render('sponsors', {
    perRow: exports.perRow,
    width: exports.imageWidth(exports.images, exports.perRow),
    images: exports.images,
  });
});

exports.imageWidth = (images, perRow) => {
  if (images.length > perRow) {
    return width / perRow;
  }
  return width / images.length;
};

app.listen(port, () => console.log(`itwkracing-obs:web listening: http://localhost:${port}`));
