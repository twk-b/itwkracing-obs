var _ = require('lodash');
var irsdk = require('node-irsdk');


irsdk.init({
  telemetryUpdateInterval: 1000,
  sessionInfoUpdateInterval: 1000,
});

var iracing = irsdk.getInstance();
var current = {};

var States = iracing.Consts.CameraState
console.log('States', States);
var state = States.CamToolActive | States.UIHidden; // | States.UseMouseAimMode


exports.init = () => {
  iracing.camControls.setState(state);
  console.log('state set');
  //iracing.camControls.switchToCar(3);
  // iracing.camControls.switchToCar('02')

  // show leader
  // show car #2 using cam group 3
  //iracing.camControls.switchToCar(2, 3)
};


var inc = 160;
exports.start = () => {
  console.log('start');
  iracing.playbackControls.play()
  iracing.camControls.switchToCar('leader');
  iracing.playbackControls.searchTs(2, 9 * 60 * 1000); // just before race start, watch for 30 seconds

  setTimeout(exports.next, 15 * 1000);
};

exports.next = () => {
  console.log('next', inc);
  iracing.playbackControls.search('nextIncident');
  inc--;
  /*
  iracing.playbackControls.pause()
  iracing.playbackControls.fastForward(2) // 2-16
  iracing.playbackControls.rewind()
  */
  if (inc > 0) {
    setTimeout(exports.next, 5 * 1000);
  } else {
    exports.stop();
  }
};

exports.stop = () => {
  console.log('finished');
  iracing.playbackControls.pause();
  process.exit(1);
};

iracing.on('Connected', (evt) => {
  console.log('start', evt);
  exports.start();
});

iracing.on('Disconnect', (evt) => {
  console.log('stop', evt);
  exports.stop();
});

iracing.on('SessionInfo', (evt) => {
  console.log('SI', evt);
  console.log('SI SessionInfo.sessions', evt.data.SessionInfo.Sessions);
  console.log('SI split', evt.data.SplitTimeInfo);
  current.SI = evt;
});

iracing.on('Telemetry', (evt) => {
  current.T = evt;
  if (current.SI) {
    var session = current.SI.data.SessionInfo.Sessions[evt.values.SessionNum]
    console.log('T', evt.values.SessionNum, evt.timestamp, evt.values.Lap, '/', session.SessionLaps, session.SessionType);
    if (parseInt(evt.values.Lap, 10) > parseInt(session.sessionLaps, 10)) {
      console.log('race over');
      exports.stop();
    }
  }
});

// jump to 2nd minute of 3rd session
//iracing.playbackControls.searchTs(2, 2 * 60 * 1000)
//iracing.playbackControls.searchFrame(1, 'current')

exports.init();
//exports.start();