const OBSWebSocket = require('obs-websocket-js');
const _ = require('lodash');
const axios = require('axios');
const irsdk = require('node-irsdk');

const obs = new OBSWebSocket();

irsdk.init({ telemetryUpdateInterval: 1000 });
const iracing = irsdk.getInstance();

let ads = [];
let current = {
  T: undefined,
  SI: undefined,
  scene: undefined,
  previous: undefined,
  scenes: [],
  connected: false,
};

// obs.connect({ address: 'localhost:4444', password: '$up3rSecretP@ssw0rd' });
// obs.send('RequestName', {args}) // returns Promise

// Callback API
// obs.sendCallback('RequestName', {args}, callback(err, data)) // no return value
// obs.disconnect();
const callback = console.info;
const processAds = (scenes) => {
  _.forEach(scenes, (scene) => {
    if (_.startsWith(scene.name, 'ad_') || _.startsWith(scene.name, 'adlive_')) {
      const timeSplit = _.split(_.split(scene.name, '_')[1], '@');
      ads.push({
        scene: scene.name,
        startSecond: parseInt(timeSplit[1], 10) * 60,
        duractionSeconds: parseInt(timeSplit[0], 10)
      });
    }
  });
  console.log('ads', ads);
};

obs.on('EventName', callback.bind(this, 'event'));

// The following are additional supported events.
obs.on('ConnectionOpened', () => {
  console.log(this, 'event opened');
  current.connected = true;
});
obs.on('ConnectionClosed', (evt) => {
  console.error('OBS event closed, retrying in 5s', evt);
  current.connected = false;
  setTimeout(exports.connect, 5000);
});
obs.on('AuthenticationSuccess', callback.bind(this, 'event auth'));
obs.on('AuthenticationFailure', callback.bind(this, 'event auth failure'));
obs.on('SwitchScenes', (data) => {
  current.previous = current.scene;
  current.scene = data.sceneName;
  console.info(`New Active Scene: ${data.sceneName}`);
});
obs.on('error', (err) => {
  console.error('socket error:', err);
});

exports.connect = function () {
  return obs.connect({
    address: 'localhost:4444',
    password: '$up3rSecretP@ssw0rd',
  }).then(() => {
    console.info('Success! We\'re connected & authenticated.');

    return obs.send('GetVersion');
  }).then((data) => {
    console.debug('version', _.omit(data, ['available-requests', 'availableRequests']));
    current.version = _.omit(data, ['available-requests', 'availableRequests']);
    return obs.send('GetVideoInfo');
  }).then((data) => {
    console.debug('GetVideoInfo', data);
    current.VideoInfo = data;
    return obs.send('GetCurrentScene');
  })
    .then((data) => {
      console.info('GetCurrentScene', _.omit(data, ['sources']), 'sources', _.map(data.sources, 'name').join(','));
      current.SceneList = data;
      return obs.send('GetSceneList');
    })
    .then((data) => {
      console.info(`scenes: ${_.map(data.scenes, 'name').join(', ')}`);
      console.info('setting to first scene');
      processAds(data.scenes);
      return obs.send('SetCurrentScene', {
        'scene-name': data.scenes[0].name,
      });
    })
    .catch((err) => { // Promise convention dictates you have a catch on every chain.
      console.error(err);
    });
};

// You must add this handler to avoid uncaught exceptions.
obs.on('error', (err) => {
  console.error('socket error:', err);
});

iracing.on('Telemetry', (evt) => {
  // console.log('T', evt);

  current.lastTelemetry = new Date();
  current.T = evt;
  return exports.setScene(current);
});

iracing.on('SessionInfo', (evt) => {
  if (!current.SI) {
    // send driver info to web server for driverId
    axios.post('http://localhost:3000/driver', evt.data.DriverInfo);
  }

  // console.log('SI', evt.data);
  // console.log('SI W', evt.data.WeekendInfo);
  // console.log('SI S', evt.data.SessionInfo);
  // console.log('SI D', _.keys(evt.data.DriverInfo));
  // console.log('SI', evt.data.DriverInfo);
  current.SI = evt;
  return exports.setScene(current);
});

iracing.on('Connected', (evt) => {
  current.connected = true;
  console.log('evt', evt);
  exports.StartStreaming();
});

exports.StartStreaming = () => {
  current.T = undefined;
  current.SI = undefined;
  current.previous = undefined;
  current.scene = 'Intro';

  console.log(`setting to first scene: ${current.scene}`);
  return obs.send('SetCurrentScene', {
    'scene-name': current.scene,
  }).then(() => {
    console.log('START stream...');
    return obs.send('StartStreaming');
  }).catch(console.error);
};

exports.StopStreaming = () => {
  console.log('STOP stream...');
  return obs.send('StopStreaming').catch(console.error);
};

iracing.on('Disconnected', () => {
  current.T = undefined;
  current.SI = undefined;
  current.previous = current.scene;
  current.scene = 'Final';
  current.connected = false;

  console.info(`new scene: ${current.scene}`);
  return obs.send('SetCurrentScene', {
    'scene-name': current.scene,
  }).then(() => {
    setTimeout(exports.StopStreaming, 30 * 1000);
  });
});

exports.setScene = () => {
  let newScene = 'Intro';
  if (current && current.T) {
    if (current.T.values.IsOnTrack) {
      newScene = 'DrivingCar';
    } else if (current.T.values.IsInGarage || current.T.values.PlayerCarInPitStall) {
      newScene = 'Garage';
    }
  } else {
    newScene = 'Intro';
  }

  if (current.scene !== newScene) {
    current.previous = current.scene;
    current.scene = newScene;

    if (current.T) {
      console.info('T IsInGarage', current.T.values.IsInGarage);

      console.info('T OnPitRoad', current.T.values.OnPitRoad);
      console.info('T PlayerCarInPitStall', current.T.values.PlayerCarInPitStall);
      console.info('T PlayerCarPitSvStatus', current.T.values.PlayerCarPitSvStatus);
      console.info('T CarIdxOnPitRoad', current.T.values.CarIdxOnPitRoad[0]);

      console.info('T IsOnTrack', current.T.values.IsOnTrack);
      console.info('T CarIdxTrackSurface', current.T.values.CarIdxTrackSurface[0]);
      console.info('T CarIdxTrackSurfaceMaterial', current.T.values.CarIdxTrackSurfaceMaterial[0]);
    }

    console.log('new scene:', newScene);
    return obs.send('SetCurrentScene', {
      'scene-name': newScene,
    });
  }
  return Promise.resolve();
};

// GetTransitionList
// TakeSourceScreenshot
// GetStreamingStatus
// GetStreamSettings

exports.connect();

const express = require('express');
const app = express();
app.use((err, req, res, next) => {
  console.error('err', err);
  next();
});

app.get('/', (req, res) => res.json({ message: 'itwkracing-obs:obs running', current }));
app.get('/start', (req, res) => {
  exports.StartStreaming().then((data) => {
    return res.json({ message: 'started', data });
  });
});
app.get('/stop', (req, res) => {
  exports.StopStreaming().then((data) => {
    return res.json({ message: 'stopped', data });
  });
});

const port = 3001;
app.listen(port, () => console.log(`itwkracing-obs:obs listening: http://localhost:${port}`));